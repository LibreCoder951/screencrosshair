#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

int main(int argc, char **argv)
{
    if (!glfwInit())
    {
        std::cout << "Failed to initialize GLFW." << std::endl;
        return -1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_FOCUSED, GLFW_FALSE);
    glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_FALSE);
    glfwWindowHint(GLFW_FLOATING, GLFW_TRUE);
    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_MOUSE_PASSTHROUGH, GLFW_TRUE);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    glfwSwapInterval(1);

    GLFWwindow *window = glfwCreateWindow(40, 40, "crosshair", nullptr, nullptr);
    if (!window)
    {
        std::cout << "Failed to create a window." << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    /*GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    int monWidth, monHeight;
    glfwGetMonitorPhysicalSize(monitor, &monWidth, &monHeight);
    glfwSetWindowPos(window, monWidth / 2 - 20, monHeight / 2 - 20);*/
    glfwSetWindowPos(window, 940, 520);
    glfwShowWindow(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        glfwDestroyWindow(window);
        glfwTerminate();
        return -1;
    }

    float vertices[] =
    {
        0.0f, -0.25f, 0.0f, 0.25f,
        -0.25f, 0.0f, 0.25f, 0.0f,
    };

    const char *vertexShaderSource =
        "#version 330 core\n"
        "layout (location = 0) in vec2 pos;"
        "void main()"
        "{"
        "    gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);"
        "}\0";
    const char *fragmentShaderSource =
        "#version 330 core\n"
        "out vec4 colour;"
        "void main()"
        "{"
        "    colour = vec4(0.0f, 1.0f, 1.0f, 0.0f);"
        "}\0";

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glLineWidth(2.0f);

    unsigned int VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    unsigned int vert = glCreateShader(GL_VERTEX_SHADER), frag = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(vert, 1, &vertexShaderSource, NULL);
    glShaderSource(frag, 1, &fragmentShaderSource, NULL);
    glCompileShader(vert); glCompileShader(frag);
    unsigned int shader = glCreateProgram();
    glAttachShader(shader, vert); glAttachShader(shader, frag);
    glLinkProgram(shader);
    glDeleteShader(vert); glDeleteShader(frag);

    glUseProgram(shader);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    while (!glfwWindowShouldClose(window))
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glDrawArrays(GL_LINES, 0, 4);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

