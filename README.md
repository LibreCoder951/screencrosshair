# Screen Crosshair

This program displays a cyan crosshair in the middle of your screen (only the middle on a 16:9 1080p monitor)

This program is licensed under the MIT license, which you can find at https://mit-license.org/

